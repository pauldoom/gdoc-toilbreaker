/**
 * Loads the list of files for a given directory into a new empty sheet
 * in the Google Sheet specified by the "spreadsheetUrl" variable.
 *
 * You must provide the folder ID for the folder you want to mine.
 * You can get this from the URL you see when you are in
 * the folder in Drive.
 *
 * In this example:
 *
 *   https://drive.google.com/drive/folders/ROTTING-WAYS
 *
 * The folderId is "ROTTING-WAYS".
 *
 * @param {folderId} Google Drive Folder ID
 * @param {folderResourceKey} Google Drive resourcekey
 * @return None
 * @customfunction
 */

const collator = new Intl.Collator(undefined, {
  numeric: true,
  sensitivity: 'case'
});

// function importDirectoryListToSheet(folderId, spreadsheetUrl, getUrls = false) {
function importDirectoryListToSheet() {
  var folderId = "CHANGEME";
  var spreadsheetUrl = "CHANGEME";

  var spreadsheet = SpreadsheetApp.openByUrl(spreadsheetUrl);
  Logger.log("Opened sheet " + spreadsheet.getUrl());

  var sheet = spreadsheet.insertSheet('DLImport-' + folderId);

  var myFolder = DriveApp.getFolderById(folderId);
  var myFiles = myFolder.getFiles();

  var rows = [];
  while(myFiles.hasNext()) {
    var f = myFiles.next();
    if (f != null) {
      rows.push([f.getName()])
    }
  }

  Logger.log("Found " + rows.length + " files:");

  // Sort alphanumerically like Drive does
  rows.sort((a, b) => collator.compare(a, b));

  Logger.log(rows.join("\n"))

  sheet.getRange(1,1,rows.length,1).setValues(rows);
}

